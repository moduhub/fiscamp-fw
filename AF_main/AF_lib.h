#ifndef _AF_LIB_H_
#define _AF_LIB_H_

#include <stdint.h>
#include <stddef.h>
#include <SD.h>

#define SD_CARD_PIN 17

#define PRINT_DEBUG 0       /** Whether debug prints are enabled or not. */

#define MAX_RESPONSE_TIME_MS 500
#define MAX_CHARACTER_TIME_MS 7

#define SERIAL_BYPASS_SNIFF_UP 1
#define SERIAL_BYPASS_SNIFF_DOWN 2
#define SERIAL_ASSUME_UP 3
#define SERIAL_ASSUME_DOWN 4

#define RS232SEL0_PIN 5 // RS232SEL0 pin.
#define RS232SEL1_PIN 6 // RS232SEL1 pin.
#define RS232SEL2_PIN 7 // RS232SEL2 pin.
#define ALARM_PIN 8     // Alarm (AL_IO) pin.
#define TC_A_AMP_PIN 14 // TC A amplified signal pin.
#define TC_B_AMP_PIN 15 // TC B amplified signal pin.
#define TC_C_AMP_PIN 16 // TC C amplified signal pin.
#define UART_RX_PIN 0   // REMOVE -> probably not needed
#define UART_TX_PIN 1   // REMOVE -> probably not needed
#define NUM_SAMPLES 83  // Number of current samples to be taken.

#define TC_A 0
#define TC_B 1
#define TC_C 2

#define CRC16 0x8005

/**
 * @brief Word variable type.
 */
typedef union
{
    uint8_t byte[2];
    uint8_t octet[2];
    uint16_t word;
} Word_t;

/**
 * @brief DoubleWord variable type.
 */
typedef union
{
    uint8_t byte[4];
    uint8_t octet[4];
    uint16_t word[2];
    uint32_t dword;
    float cFloat;
} DoubleWord_t;

/**
 * @brief PowerMeterMeasure variable type.
 */
typedef struct
{
    float phaseVoltages[3];
    float phaseCurrents[3];
    uint8_t hours;
    uint8_t minutes;
    uint8_t seconds;
    uint8_t day;
    uint8_t month;
    uint8_t year;
} PowerMeterSnapshot_t;

/**
 * @brief OversightMeasure variable type.
 */
typedef struct
{
    float phaseCurrents[3];
} OversightMeasure_t;


void AF_initPins(void);

File AF_SDInitDatalogFile(const char *pDatalogFileName);

float AF_getRMSCurrentFromTC(uint8_t pTC);

void AF_setSerialMode(int mode);

void AF_DEBUG_PrintHexData(const char *pName, const uint8_t *pBuffer, unsigned int pBufferSize);

void AF_serialEcho(uint8_t *pBuffer);

bool AF_spyInstantValues(uint8_t *pResponseBuffer, PowerMeterSnapshot_t *pSnapshot);

bool AF_readInstantValues(uint8_t *pResponseBuffer, uint8_t *pCommandBuffer, PowerMeterSnapshot_t *pSnapshot);

int AF_storeMeasurementData(File pDatalogFile, PowerMeterSnapshot_t *pSnapshot, OversightMeasure_t *pOversight);

#endif //_AF_LIB_H_