#include "AF_lib.h"

#include "Arduino.h"
#include <stdio.h>
#include <math.h>


/**
 * @brief Calculate the RMS Voltage from the passed current samples array.
 *
 * @param pSamplesArray IN -- Current samples array.
 * @param pNumSamples IN -- Number of samples.
 * @return Calculated RMS Voltage, in mV.
 */
float _getRMSVoltage(int *pSamplesArray, size_t pNumSamples)
{
    uint32_t tSumCurrents = 0;

    for (size_t i = 0; i < pNumSamples; i++)
    {
        tSumCurrents += (uint32_t)pSamplesArray[i];
    }

    int tAverageCurrent = int(tSumCurrents / pNumSamples);

    double tSquareSum = 0.0;

    for (size_t i = 0; i < pNumSamples; i++)
    {
        tSquareSum += pow((double)(pSamplesArray[i] - tAverageCurrent), 2.0);
    }

    float tRMSVoltage_mV = (3.0f / 1.024f) * (float)sqrt((tSquareSum / (double)pNumSamples));

    #if PRINT_DEBUG
    Serial.println(F(""));
    Serial.print(F("DEBUG >> Sample: "));
    Serial.println(pSamplesArray[0]);

    Serial.print(F("DEBUG >> Sum of current values: "));
    Serial.println(tSumCurrents);

    Serial.print(F("DEBUG >> Average Voltage: "));
    double tAverageVoltage_mV = (3.0 / 1.024) * ((double)tSumCurrents / (double)pNumSamples);
    Serial.print(tAverageVoltage_mV);
    Serial.println(F(" mV"));

    Serial.print(F("DEBUG >> Average Current: "));
    Serial.print(tAverageCurrent);
    Serial.println(F(" # must be 512"));

    Serial.print(F("DEBUG >> RMS Voltage: "));
    Serial.print(tRMSVoltage_mV);
    Serial.println(F(" mV"));

    Serial.print(F("DEBUG >> Square sum: "));
    Serial.println(tSquareSum);

    Serial.print(F("DEBUG >> Square root: "));
    Serial.println(sqrt(tSquareSum / (float)pNumSamples));
    #endif

    return tRMSVoltage_mV;
}

/**
   * @brief Read current values and store them in the passed array.
 *
 * @param pSampleArray OUT -- Array to store the current samples.
 * @param pNumSamples IN -- Number of samples to store in the array.
 * @param pTCPin IN -- Pin of the TC to be read.
 */
void _readCurrentValues(int *pSampleArray, size_t pNumSamples, uint8_t pTCPin)
{
    for (size_t i = 0; i < pNumSamples; i++)
    {
        pSampleArray[i] = analogRead(pTCPin);
        delayMicroseconds(500);
    }
}

/**
 * @brief Calculates the 16-bit CRC (Cyclic Redundancy Check) for the given buffer.
 *
 * @param pBuffer IN -- Pointer to the buffer containing the data for CRC calculation.
 * @param pSize IN -- The size of the buffer in bytes.
 * @return The calculated 16-bit CRC value.
 */
uint16_t _calculateCRC16(uint8_t *pBuffer, unsigned int pSize)
{
    uint16_t out = 0x0000;
    int bits_read = 0, bit_flag;
    uint8_t *tBufferPointer = pBuffer;
    unsigned int tRemainingBytes = pSize;
    while (tRemainingBytes > 0)
    {
        bit_flag = out >> 15;
        out <<= 1;
        out |= ((*tBufferPointer) >> bits_read) & 1;
        bits_read++;
        if (bits_read > 7)
        {
            bits_read = 0;
            tBufferPointer++;
            tRemainingBytes--;
        }
        if (bit_flag)
            out ^= CRC16;
    }
    uint16_t i;
    for (i = 0; i < 16; ++i)
    {
        bit_flag = out >> 15;
        out <<= 1;
        if (bit_flag)
            out ^= CRC16;
    }
    uint16_t mirror = 0;
    i = 0x8000;
    int j = 0x0001;
    for (; i != 0; i >>= 1, j <<= 1)
    {
        if (i & out)
            mirror |= j;
    }
    return mirror;
}

/**
 * @brief Connects to a device using a serial connection.
 *
 * This function attempts to establish a connection with a device by sending and
 * receiving data over a serial connection. It assumes a specific serial mode is
 * set (SERIAL_ASSUME_DOWN) and listens for a specific response byte (0x05) within
 * the specified timeout. The function allows for multiple connection attempts if
 * the initial attempts fail.
 *
 * @param pTimeout IN -- The timeout value in milliseconds for each connection attempt.
 * @return `true` if the connection is successful, `false` otherwise.
 */
bool _connectSerial(unsigned int pTimeout)
{
    AF_setSerialMode(SERIAL_ASSUME_DOWN);
    for (unsigned int attempts = 0; attempts < 5; attempts++)
    {
        unsigned long tLastByteTime = millis();
        while (millis() - tLastByteTime < pTimeout)
        {
            if (Serial.available())
            {
                uint8_t tByte = Serial.read();
                if (tByte == 0x05)
                {
                    delay(1);
                    return true;
                }
            }
        }
        if (attempts < 4)
        {
            // pinMode(1, OUTPUT); // Set uart pin as an output
            // digitalWrite(1, LOW);
            // delay(1000);
            // digitalWrite(1, HIGH);
            Serial.write(0x00);
            delay(1000);
        }
    }
    return false;
}

/**
 * @brief Receives data from the serial input, with a timeout.
 *
 * This function receives data from the Serial input until either the buffer is full or
 * a maximum character time has elapsed. The received data is stored in the provided buffer
 * and the number of bytes received is returned.
 *
 * @param pBuffer OUT -- Pointer to the buffer where the received data will be stored.
 * @param pBufferSize IN -- The size of the buffer in bytes.
 * @return The number of bytes received and stored in the buffer.
 *         Returns 0 if the maximum response time is exceeded without receiving any data.
 */
unsigned int _receiveSerialData(uint8_t *pBuffer, unsigned int pBufferSize)
{
    unsigned int bytesReceived = 0;
    unsigned long last_byte_time = millis();
    uint8_t *tBufferPointer = pBuffer;
    uint8_t tPeekByte;
    do
    {
        while (!Serial.available())
        { // wait for a response, limited to MAX_RESPONSE_TIME_MS
            if (millis() - last_byte_time > MAX_RESPONSE_TIME_MS)
            {
                return 0;
            }
        }
        tPeekByte = Serial.peek();
        if (tPeekByte == 0x05 || tPeekByte == 0x00)
        { // discard any ENQ between command and response
            Serial.read();
        }
    } while (tPeekByte == 0x05 || tPeekByte == 0x00);
    do
    { // _receiveSerialData until the line silences for MAX_CHARACTER_TIME_MS
        if (Serial.available())
        {
            *tBufferPointer++ = Serial.read();
            bytesReceived++;
            last_byte_time = millis();
            if (bytesReceived == pBufferSize)
            {
                break;
            }
        }
    } while (millis() - last_byte_time < MAX_CHARACTER_TIME_MS);
    return bytesReceived;
}

/**
 * @brief Get the RMS current from a specified TC.
 *
 * @param pTC IN -- TC to read the RMS current from. Valid pTC are:
 *          - TC_A
 *          - TC_B
 *          - TC_C
 * @return RMS current from the specified TC, in Amps.
 */
float AF_getRMSCurrentFromTC(uint8_t pTC)
{
    int tCurrentSamples[NUM_SAMPLES] = {0};

    switch (pTC)
    {
    case TC_A:
        _readCurrentValues(tCurrentSamples, NUM_SAMPLES, TC_A_AMP_PIN);
        break;
    case TC_B:
        _readCurrentValues(tCurrentSamples, NUM_SAMPLES, TC_B_AMP_PIN);
        break;
    case TC_C:
        _readCurrentValues(tCurrentSamples, NUM_SAMPLES, TC_C_AMP_PIN);
        break;
    default:
        break;
    }

    float tRMSVoltageMilliVolts = _getRMSVoltage(tCurrentSamples, NUM_SAMPLES);
    
    // TODO: TRANSFORM VOLTAGE INTO CURRENT
    float tRMSCurrent = tRMSVoltageMilliVolts * 0.0125f;

    return tRMSCurrent;
}

/**
 * @brief Initialize the AF pins.
 *
 */
void AF_initPins(void)
{
    pinMode(TC_A_AMP_PIN, INPUT);
    pinMode(TC_B_AMP_PIN, INPUT);
    pinMode(TC_C_AMP_PIN, INPUT);
    pinMode(RS232SEL0_PIN, OUTPUT);
    pinMode(RS232SEL1_PIN, OUTPUT);
    pinMode(RS232SEL2_PIN, OUTPUT);
    pinMode(ALARM_PIN, OUTPUT);

    digitalWrite(RS232SEL0_PIN, HIGH);
    digitalWrite(RS232SEL1_PIN, HIGH);
    digitalWrite(RS232SEL2_PIN, LOW);
}

/**
 * @brief Initialize the SD card and the datalog file.
 * 
 * @param pDatalogFileName IN -- The complete name of the datalog file, e.g. "datalog.txt".
 * @return Datalog File pointer. 
 * @warning THE SUPPORT FOR LONG FILE NAMES (LFN) IS NOT ENABLED, SO THE NAME OF THE FILE 
 * IS LIMITED TO 8 CHARACTERS AND THE EXTENSION TO 3 CHARACTERS. 
 */
File AF_SDInitDatalogFile(const char *pDatalogFileName)
{
    #if PRINT_DEBUG
    Serial.println(F("DEBUG >> Initialization Sequence..."));
    Serial.println(F("DEBUG >> Initializing SD card..."));
    #endif

    unsigned int SDInitAttempts = 0;
    while (1)
    {
        if (!SD.begin(SD_CARD_PIN))
        {
            SDInitAttempts++;

            #if PRINT_DEBUG
            Serial.print(F("DEBUG >> SD Initialization attempt "));
            Serial.print(SDInitAttempts);
            Serial.println(F(" failed!"));
            #endif

            if (SDInitAttempts > 3)
            {
                #if PRINT_DEBUG
                Serial.println(F("DEBUG >> Failed to initialize SD card, AF will work without the storage function."));
                #endif
                break;
            }
            delay(100);
        }
        else
        {
            #if PRINT_DEBUG
            Serial.println(F("DEBUG >> SD Initialization successful."));
            #endif
            break;
        }
    }

    bool tFileExistedBefore = SD.exists(pDatalogFileName);

    #if PRINT_DEBUG
    Serial.print(F("DEBUG >> File existed before? "));
    Serial.println(tFileExistedBefore ? F("Yes") : F("No"));
    #endif

    File tDataloggerFile = SD.open(pDatalogFileName, O_WRITE | O_CREAT | O_APPEND);

    if (tDataloggerFile)
    {
        if (!tFileExistedBefore)
        {
            tDataloggerFile.print(F("data, hora, sv1, sv2, sv3, si1, si2, si3, oi1, oi2, oi3\n"));
        }
        #if PRINT_DEBUG
        Serial.println(F("DEBUG >> datalog.txt file created."));
        #endif
    }
    return tDataloggerFile;
}

/**
 * @brief Sets the serial communication mode.
 *
 * The mode determines the configuration of the RS232SEL pins and controls the direction
 * of communication for the serial interface.
 *
 * @param mode IN -- The mode to set for the serial communication. Valid modes are:
 *        - SERIAL_BYPASS_SNIFF_UP
 *        - SERIAL_BYPASS_SNIFF_DOWN
 *        - SERIAL_ASSUME_UP
 *        - SERIAL_ASSUME_DOWN
 *
 * @note This function assumes that the RS232SEL pins and Serial communication are already initialized.
 */
void AF_setSerialMode(int mode)
{
    Serial.flush();
    switch (mode)
    {
    default:
    case SERIAL_BYPASS_SNIFF_UP:
    {
        digitalWrite(RS232SEL0_PIN, LOW);
        digitalWrite(RS232SEL1_PIN, LOW);
        digitalWrite(RS232SEL2_PIN, LOW);
    }
    break;
    case SERIAL_BYPASS_SNIFF_DOWN:
    {
        digitalWrite(RS232SEL0_PIN, LOW);
        digitalWrite(RS232SEL1_PIN, LOW);
        digitalWrite(RS232SEL2_PIN, HIGH);
    }
    break;
    case SERIAL_ASSUME_UP:
    { // warning, tx up will pass to rx down
        digitalWrite(RS232SEL0_PIN, HIGH);
        digitalWrite(RS232SEL1_PIN, LOW);
        digitalWrite(RS232SEL2_PIN, LOW);
    }
    break;
    case SERIAL_ASSUME_DOWN:
    { // warning, tx down will pass to rx up
        digitalWrite(RS232SEL0_PIN, LOW);
        digitalWrite(RS232SEL1_PIN, HIGH);
        digitalWrite(RS232SEL2_PIN, HIGH);
    }
    break;
    }
    delay(1);
}

/**
 * @brief Prints a hexadecimal representation of data buffer for debugging purposes.
 *
 * This function prints the hexadecimal representation of a data buffer, along with its size
 * and a provided name, to the Serial output. The buffer is printed in rows of 16 bytes, with
 * each byte represented by two hexadecimal digits.
 *
 * @param pName IN -- The name or identifier associated with the data buffer.
 * @param pBuffer IN -- Pointer to the data buffer to be printed.
 * @param pBufferSize IN -- The size of the data buffer in bytes.
 *
 * @note This function assumes that the Serial communication is already initialized and available.
 */
void AF_DEBUG_PrintHexData(const char *pName, const uint8_t *pBuffer, unsigned int pBufferSize)
{
    Serial.print(F("DEBUG >> ["));
    Serial.print(pBufferSize);
    Serial.print(F(" bytes] "));
    Serial.println(pName);
    for (unsigned int i = 0; i < pBufferSize; i++)
    {
        if (pBuffer[i] < 0x10)
        {
            Serial.print(0, HEX);
        }
        Serial.print(pBuffer[i], HEX);
        if ((i + 1) % 16 == 0)
            Serial.println();
        else if ((i + 1) % 8 == 0)
            Serial.print(" ");
    }
    Serial.println();
}

/**
 * @brief Reads characters from the Serial input and echoes them back.
 *
 * This function reads characters from the Serial input until a newline character
 * ('\n') is received or until 32 characters have been received, whichever comes first.
 * The received characters are stored in a buffer and then echoed back to the Serial output.
 * 
 * @param pBuffer IN -- 
 */
void AF_serialEcho(uint8_t *pBuffer)
{
    int bytesReceived = 0;
    while (bytesReceived < 32)
    {
        while (!Serial.available())
            ;
        uint8_t tReceivedChar = Serial.read();
        if (tReceivedChar == '\n')
        {
            pBuffer[bytesReceived] = 0;
            break;
        }
        else
        {
            pBuffer[bytesReceived] = tReceivedChar;
        }
        bytesReceived++;
    }
    #if PRINT_DEBUG
    Serial.print(F("DEBUG >> Echo: "));
    Serial.print((char *)pBuffer);
    Serial.println();
    #endif
}

/**
 * @brief Spies instant values from a power meter and populates the provided snapshot.
 *
 * This function spies on instant values from a power meter by receiving data from a serial connection
 * in SERIAL_BYPASS_SNIFF_DOWN mode. It receives the data into a buffer and verifies the integrity of
 * the received data by calculating the CRC-16 checksum. If the received data is valid and matches the
 * expected format, the instant values are extracted and stored in the provided `PowerMeterSnapshot_t`
 * structure.
 *
 * @param pResponseBuffer IN -- Pointer to the response buffer.
 * @param pSnapshot OUT -- Pointer to the `PowerMeterSnapshot_t` structure where the instant values will be stored.
 * @return `true` if the instant values are successfully spied and stored, `false` otherwise.
 */
bool AF_spyInstantValues(uint8_t *pResponseBuffer, PowerMeterSnapshot_t *pSnapshot)
{
    AF_setSerialMode(SERIAL_BYPASS_SNIFF_DOWN);
    unsigned int tBytesReceived = _receiveSerialData(pResponseBuffer, 258);
    if (tBytesReceived > 0)
    {
        Word_t tCRC;
        tCRC.word = _calculateCRC16(pResponseBuffer, 256);
        if (pResponseBuffer[0] == 0x14 && pResponseBuffer[256] == tCRC.byte[0] && pResponseBuffer[257] == tCRC.byte[1])
        {
            memcpy(pSnapshot->phaseVoltages, pResponseBuffer + 15, 12);
            memcpy(pSnapshot->phaseCurrents, pResponseBuffer + 39, 12);
            pSnapshot->hours = pResponseBuffer[5];
            pSnapshot->minutes = pResponseBuffer[6];
            pSnapshot->seconds = pResponseBuffer[7];
            pSnapshot->day = pResponseBuffer[8];
            pSnapshot->month = pResponseBuffer[9];
            pSnapshot->year = pResponseBuffer[10];
            
            return true;
        }
    }
    return false;
}

/**
 * @brief Reads instant values from a power meter and populates the provided snapshot.
 *
 * This function reads instant values from a power meter by sending a specific command
 * over a serial connection. It assumes a specific serial mode is set (SERIAL_ASSUME_DOWN)
 * and establishes a connection with the power meter. If the connection is successful, the
 * command is sent, and the response is received and processed. If the response is valid,
 * the instant values are extracted and stored in the provided `PowerMeterSnapshot_t` structure.
 * 
 * @param pResponseBuffer IN -- Pointer to the response buffer.
 * @param pCommandBuffer IN -- Pointer to the command buffer.
 * @param pSnapshot OUT -- Pointer to the `PowerMeterSnapshot_t` structure where the instant values will be stored.
 * @return `true` if the instant values are successfully read and stored, `false` otherwise.
 */
bool AF_readInstantValues(uint8_t *pResponseBuffer, uint8_t *pCommandBuffer, PowerMeterSnapshot_t *pSnapshot)
{
    AF_setSerialMode(SERIAL_ASSUME_DOWN);
    if (_connectSerial(500))
    {
        Word_t tCRC;
        memset(pCommandBuffer, 0, 66);
        pCommandBuffer[0] = 0x14;
        pCommandBuffer[1] = 0;
        pCommandBuffer[2] = 0;
        pCommandBuffer[3] = 0;
        tCRC.word = _calculateCRC16(pCommandBuffer, 64);
        pCommandBuffer[64] = tCRC.byte[0];
        pCommandBuffer[65] = tCRC.byte[1];
        Serial.write(pCommandBuffer, 66);
        Serial.flush();
        unsigned int tBytesReceived = _receiveSerialData(pResponseBuffer, 258);
        if (tBytesReceived > 0)
        {
            // AF_setSerialMode(SERIAL_ASSUME_UP);
            // AF_DEBUG_PrintHexData("Command", pCommandBuffer, 66);
            // AF_DEBUG_PrintHexData("Response", pResponseBuffer, tBytesReceived);
            if (pResponseBuffer[0] == 0x14)
            {
                memcpy(pSnapshot->phaseVoltages, pResponseBuffer + 15, 12);
                memcpy(pSnapshot->phaseCurrents, pResponseBuffer + 39, 12);
                return true;
            }
        }
    }
    return false;
}

/**
 * @brief Stores power meter and oversight measurements to the datalogger file.
 *
 * This function stores the power meter measurements and oversight data to the datalogger file,
 * if the file is available for writing. The measurements include phase voltages and currents from
 * the PowerMeterSnapshot_t, as well as phase currents from the OversightMeasure_t structure. The data
 * is written as comma-separated values (CSV) format with each measurement on a new line.
 *
 * @param pDatalogFile IN -- Datalog file pointer. 
 * @param pSnapshot IN -- Pointer to the PowerMeterSnapshot_t containing the phase voltages and currents.
 * @param pOversight IN -- Pointer to the OversightMeasure_t containing the phase currents.
 * @return 0 on successful storage, indicating the data was written to the datalogger file.
 */
int AF_storeMeasurementData(File pDatalogFile, PowerMeterSnapshot_t *pSnapshot, OversightMeasure_t *pOversight)
{
    if (pDatalogFile)
    {
        pDatalogFile.print(pSnapshot->day < 16 ? F("0") : F(""));
        pDatalogFile.print(pSnapshot->day, HEX);
        pDatalogFile.print(pSnapshot->month < 16 ? F("/0") : F("/"));
        pDatalogFile.print(pSnapshot->month, HEX);
        pDatalogFile.print(F("/20"));
        pDatalogFile.print(pSnapshot->year, HEX);
        pDatalogFile.print(F(", "));
        pDatalogFile.print(pSnapshot->hours < 16 ? F("0") : F(""));
        pDatalogFile.print(pSnapshot->hours, HEX);
        pDatalogFile.print(F(":"));
        pDatalogFile.print(pSnapshot->minutes < 16 ? F("0") : F(""));
        pDatalogFile.print(pSnapshot->minutes, HEX);
        pDatalogFile.print(F(":"));
        pDatalogFile.print(pSnapshot->seconds < 16 ? F("0") : F(""));
        pDatalogFile.print(pSnapshot->seconds, HEX);
        pDatalogFile.print(F(", "));
        pDatalogFile.print(pSnapshot->phaseVoltages[0]);
        pDatalogFile.print(F(", "));
        pDatalogFile.print(pSnapshot->phaseVoltages[1]);
        pDatalogFile.print(F(", "));
        pDatalogFile.print(pSnapshot->phaseVoltages[2]);
        pDatalogFile.print(F(", "));
        pDatalogFile.print(pSnapshot->phaseCurrents[0]);
        pDatalogFile.print(F(", "));
        pDatalogFile.print(pSnapshot->phaseCurrents[1]);
        pDatalogFile.print(F(", "));
        pDatalogFile.print(pSnapshot->phaseCurrents[2]);
        pDatalogFile.print(F(", "));
        pDatalogFile.print(pOversight->phaseCurrents[0]);
        pDatalogFile.print(F(", "));
        pDatalogFile.print(pOversight->phaseCurrents[1]);
        pDatalogFile.print(F(", "));
        pDatalogFile.print(pOversight->phaseCurrents[2]);
        pDatalogFile.print(F("\n"));
        pDatalogFile.flush();
        #if PRINT_DEBUG
        Serial.println(F("DEBUG >> Data stored"));
        #endif
    }
    else
    {
        #if PRINT_DEBUG
        Serial.print(F("DEBUG >> Could not store data into SD card. pDatalogFile: "));
        Serial.println(pDatalogFile);
        #endif
    }

    return 0;
}