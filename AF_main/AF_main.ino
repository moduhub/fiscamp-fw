#include "AF_lib.h"

#define DATALOGGER_FILE_NAME "log.txt"

#define DEBUG_MODE 1  /** Whether debug serial prints are printed to provide debug and health information about the AF */

File gDataloggerFile;

PowerMeterSnapshot_t gPowerMeterSnapshot;
OversightMeasure_t gOversightMeasure;

uint8_t gSerialBuffer[32];
uint8_t gCommandBuffer[66];
uint8_t gResponseBuffer[258];

void setup()
{
	memset(&gPowerMeterSnapshot, 0, sizeof(PowerMeterSnapshot_t));
	memset(&gOversightMeasure, 0, sizeof(OversightMeasure_t));
	
	AF_initPins();

	AF_setSerialMode(SERIAL_ASSUME_UP);

	// Open serial communications and wait for port to open:
	Serial.begin(9600);
	
	#if DEBUG_MODE
	Serial.println(F(""));
	Serial.println(F("*************** Amperimetro Fiscal v2.0.0 ***************"));
	#endif

	gDataloggerFile = AF_SDInitDatalogFile(DATALOGGER_FILE_NAME);

	#if DEBUG_MODE
	if(gDataloggerFile){
        Serial.println(F("INFO >> SD initialization successful."));
	} else {
        Serial.println(F("WARN >> Failed to open the datalogger file. Data will not be stored."));
	}
	#endif
}


void loop()
{
	AF_setSerialMode(SERIAL_BYPASS_SNIFF_DOWN);
	
	
	if (AF_spyInstantValues(gResponseBuffer, &gPowerMeterSnapshot))
	{
		AF_setSerialMode(SERIAL_ASSUME_UP);

		gOversightMeasure.phaseCurrents[0] = AF_getRMSCurrentFromTC(TC_A);
		gOversightMeasure.phaseCurrents[1] = AF_getRMSCurrentFromTC(TC_B);
		gOversightMeasure.phaseCurrents[2] = AF_getRMSCurrentFromTC(TC_C);

		#if DEBUG_MODE
		Serial.println(F(""));
		Serial.println(F("DEBUG >> Power Meter Snapshot:"));
		Serial.print(F("V1: "));
		Serial.print(gPowerMeterSnapshot.phaseVoltages[0]);
		Serial.print(F("  V2: "));
		Serial.print(gPowerMeterSnapshot.phaseVoltages[1]);
		Serial.print(F("  V3: "));
		Serial.println(gPowerMeterSnapshot.phaseVoltages[2]);
		Serial.print(F("I1: "));
		Serial.print(gPowerMeterSnapshot.phaseCurrents[0]);
		Serial.print(F("  I2: "));
		Serial.print(gPowerMeterSnapshot.phaseCurrents[1]);
		Serial.print(F("  I3: "));
		Serial.println(gPowerMeterSnapshot.phaseCurrents[2]);
		Serial.println(F(""));
		Serial.println(F("DEBUG >> AF Current Measurements: "));
		Serial.print(F("I1: "));
		Serial.print(gOversightMeasure.phaseCurrents[0]);
		Serial.print(F("  I2: "));
		Serial.print(gOversightMeasure.phaseCurrents[1]);
		Serial.print(F("  I3: "));
		Serial.println(gOversightMeasure.phaseCurrents[2]);
		#endif

		if(!gDataloggerFile) gDataloggerFile = AF_SDInitDatalogFile(DATALOGGER_FILE_NAME);

		if(gDataloggerFile){
			AF_storeMeasurementData(gDataloggerFile, &gPowerMeterSnapshot, &gOversightMeasure);
		} else {
			#if DEBUG_MODE
			Serial.println(F("*** ERROR: Data can't be stored!"));
			#endif
		}
	}
	else
	{
		// AF_setSerialMode(SERIAL_ASSUME_UP);
		// Serial.println(F("*** ERROR: Read failed."));
	}
}
